import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {Router} from "@angular/router";
import {MessageService} from "../../../service/message.service";
import {AdminServiceService} from "../../../service/admin-service.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UserService} from "../../../service/user-service.service";

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
name= 'admin';
picture= 'https://www.samaauto.sn/img/logos/admin.png'
  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';

  userMenu = [ { title: 'Log out'} ];
  message: any;
  subscription: Subscription;

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private userService: UserData,
              private service:UserService,
              private layoutService: LayoutService,
              private adminService:AdminServiceService,
              private router:Router,private dialog:MatDialog,
              private breakpointService: NbMediaBreakpointsService,private messageService:MessageService) {
   this.service.getTOKEN();





  }

  ngOnInit() {

    this.message=JSON.parse(localStorage.getItem("user"));
    this.adminService.getAdminById(this.message.admin_id).subscribe((res:any)=>{
      this.name=res.username;
    this.message=res.userPicture;
    }
    );
    this.currentTheme = this.themeService.currentTheme;

    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => this.user = users.nick);

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);

  this.messageService.getMessage().subscribe((msg:any)=>{
 this.message=msg.token;
  console.log(msg)})
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }
  logOut(){
    localStorage.clear();
   this.router.navigate(['/auth/login']);
  }

  gettoken() {
 this.service.getTOKEN();
    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.message = message.token;
      this.name=message.userName});
  }
}
