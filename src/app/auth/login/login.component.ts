import { Router } from '@angular/router';
import {AuthService, User} from '../auth-service.service';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { NB_AUTH_OPTIONS } from '@nebular/auth';

import {Component, Inject, OnInit} from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import {FormControl, Validators} from "@angular/forms";
import {GlobalsService} from "../../service/globals.service";
import {UserService} from "../../service/user-service.service";
import {EthereumProvider} from "../../ethereumProvider/ethereum";

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent {


  redirectDelay: number = 0;

  errors: string[] = [];
  messages: string[] = [];
  user: any = {rememberMe: true};

  showMessages: any = {};
  submitted: boolean = false;

  validation = {};

  constructor(public globalService:GlobalsService,protected auth: AuthService, private service:UserService,protected router: Router,
              private web3js:EthereumProvider) {
    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');

    this.validation = this.getConfigValue('forms.validation');
  }

  loginEmail() {
    this.errors = this.messages = [];
    this.submitted = true;
    console.log("hi")
    this.redirectToDashboard();
    let data=this.web3js.verify(this.user.password);
    console.log("hi")
    this.service.login(this.user.email,data).subscribe(res=>

    {console.log(res)
localStorage.setItem("user","hhh")
    this.redirectToDashboard()},error1 =>       this.errors = ['invalid Private Key || email']

  )  ;

    if(this.user.password.length<66)
    {
      this.errors = ['invalid Private Key'];
this.submitted=false;
    }
    else {

     // localStorage.setItem("prvKey", this.user.password);
      //localStorage.setItem("user", this.user.email);
      this.submitted = false;
    }
   /* this.auth.login(this.user.email, this.user.password)

      .then(response => {

        localStorage.setItem("user", (JSON.stringify(response)));
this.globalService.setUser(response);
      }).catch(error => {
      this.submitted = false;
      this.errors = ['invalid email or password'];
    });*/

  }



  loginSocial(name) {

  }

  loginGoogle() {
  }

  loginFb() {
  }

  redirectToDashboard() {
    setTimeout(() => {
      this.router.navigate(['/sdjflmdsjflsdjfmlkdsflmkdsmjl/ali']);
    }, this.redirectDelay);
  }

  getConfigValue(key: string): any {
  }

  ngOnInit(): void {
  }
}

