import { Router } from '@angular/router';
import {AuthService, User} from '../auth-service.service';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { NB_AUTH_OPTIONS } from '@nebular/auth';

import * as saveAs from 'file-saver';

import {Component, Inject, OnInit} from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import {FormControl, Validators} from "@angular/forms";
import {GlobalsService} from "../../service/globals.service";
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {timestamp} from "rxjs/operators";
import {UserService} from "../../service/user-service.service";

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxRegisterComponent {


  redirectDelay: number = 0;

  errors: string[] = [];
  messages: string[] = [];
  user: any = {rememberMe: true};

  showMessages: any = {};
  submitted: boolean = false;

  validation = {};

  constructor(private web3js:EthereumProvider,protected router: Router,private  service:UserService) {
    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');

    this.validation = this.getConfigValue('forms.validation');
  //console.log(this.web3js.verify());
  }

  loginEmail() {
    this.errors = this.messages = [];
    this.submitted = true;

    this.web3js.generateAccount();

    this.service.addUser(this.user.email,this.web3js.getAccount()).subscribe(res=>{console.log(res)

      this.redirectToDashboard();
      let data=JSON.stringify({address:this.web3js.getAccount()})+"\n";
      data +=JSON.stringify({ privateKey:this.web3js.getPrivateKey()});
      this.createFile(data);

    },error1 => {
      this.errors = ['invalid  email']
      console.log(error1)
    });


       localStorage.setItem('pubKey',this.web3js.getAccount());
        localStorage.setItem("prvKey",this.web3js.getPrivateKey());
    }



  loginSocial(name) {

  }

  loginGoogle() {
  }

  loginFb() {
  }

  redirectToDashboard() {
    setTimeout(() => {
      this.router.navigate(['/sdjflmdsjflsdjfmlkdsflmkdsmjl/ali']);
    }, this.redirectDelay);
  }

  getConfigValue(key: string): any {
  }

  ngOnInit(): void {
  }

  createFile(data) {
    var timestamp = Math.round(+new Date()/1000);

    let file = new Blob([data], { type: 'text/text;charset=utf-8' });
    saveAs(file, timestamp+'key.txt')

  }


}

