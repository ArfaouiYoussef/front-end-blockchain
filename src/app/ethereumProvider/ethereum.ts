
import { Injectable } from '@angular/core';
// @ts-ignore
const  abi=require("../contract/ABI.json")
const  transportAbi=require("../contract/transport.json")
const  covoiturageabi=require("../contract/covoiturage.json")
const  Restaurantabi=require("../contract/restaurant.json")
const  bourseAbi=require("../contract/school.json")
const  foyerAbi=require("../contract/foyer.json")


import _ from 'lodash';
import HDKey from "hdkey";
import { AppConfig } from '../../../env';
const crypto = require("crypto");
const eccrypto = require("eccrypto");

const Web3 =require('web3');
//ldld
@Injectable()
export class EthereumProvider {
  private web3: any;
  private root: HDKey.HDKey;
  private accountAddress: string;
  private privateKey: string;
  private publicKey: string;
  private mnemonic: string;

  constructor(
  ) {
    this.web3 = new Web3(new Web3.providers.HttpProvider("http://5.135.52.74:8501/"));
    this.web3.eth.net.getNetworkType(function(err, res){
      console.log("Network Type: "+res);
    });
    this.accountAddress = AppConfig.ethereum.account;
    this.privateKey = AppConfig.ethereum.privateKey;
  }

  public getValue (key: string) {
    return _.get(this.web3, key);
  }



  public generateAccount() {
    const account = this.web3.eth.accounts.create();

    this.accountAddress = account.address;
    this.privateKey = account.privateKey;



  }
  public subscribeRestaurant(type,nbmoins,amount){

    const contractTest = new this.web3.eth.Contract(transportAbi, "0x61aF9364e0fb0Fbf11F6af70604b99a7bF07356C");

    let encoded = contractTest.methods.Subscribe(this.web3.utils.fromAscii(type),amount,nbmoins).encodeABI();
    var tx = {
      to : "0x61aF9364e0fb0Fbf11F6af70604b99a7bF07356C",
      value: '0',
      gas: 2000000,
      gasPrice: '0',

      chainId: 1515,

      data : encoded
    };





    return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());


  }

public balance(){




}

  public subscribeResto(nbmoins) {

    const contractTest = new this.web3.eth.Contract(Restaurantabi, "0x73a2D5Bd0616d4bE905acfc321B3A9628984d5DD");

    let encoded = contractTest.methods.Subscribe(nbmoins).encodeABI();
    var tx = {
      to: "0x73a2D5Bd0616d4bE905acfc321B3A9628984d5DD",
      value: '0',
      gas: 2000000,
      gasPrice: '0',

      chainId: 1515,

      data: encoded
    };
    return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());

  }

  public checkUser(){

      const contractTest = new this.web3.eth.Contract(covoiturageabi, "0x660b883c119262C6b44cA9485d59Cfd9c0Ea5Fc3");

      let encoded = contractTest.methods.users(localStorage.getItem("pubKey").toString()).encodeABI();
      var tx = {
        to : "0x660b883c119262C6b44cA9485d59Cfd9c0Ea5Fc3",
        value: '0',
        gas: 2000000,
        gasPrice: '0',

        chainId: 1515,

        data : encoded
      };


    return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());

    }
  public getBourse(id){
    const contractTest = new this.web3.eth.Contract(bourseAbi, "0x4B07200fd0Cc76D2150cfd4D373B41A94D2FeBF6");

    let encoded = contractTest.methods.depositeScholarship(id,localStorage.getItem("pubKey").toString()).encodeABI();
    var tx = {
      to : "0x4B07200fd0Cc76D2150cfd4D373B41A94D2FeBF6",
      value: '0',
      gas: 2000000,
      gasPrice: '0',

      chainId: 1515,

      data : encoded
    };


    return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());


  }
  public getTokenBalance() {
    const contract = new this.web3.eth.Contract(abi, "0x92eE8f9276633822266E515047e1d42CF29bc4a6");

   return  contract.methods.balanceOf(localStorage.getItem("pubKey")).call();



  }
  public addUserCovoiturage(type){
    const contractTest = new this.web3.eth.Contract(covoiturageabi, "0x660b883c119262C6b44cA9485d59Cfd9c0Ea5Fc3");

    let encoded = contractTest.methods.Adduser(this.web3.utils.fromAscii(type)).encodeABI();
    var tx = {
      to : "0x660b883c119262C6b44cA9485d59Cfd9c0Ea5Fc3",
      value: '0',
      gas: 4000000,
      gasPrice: '0',

      chainId: 1515,

      data : encoded
    };


    return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());


  }
  public subscribeFoyer(nbmoins){

      const contractTest = new this.web3.eth.Contract(foyerAbi, "0xeD93Ff78E86eE9a7E9D3d7EB5424668BF7bcf929");

      let encoded = contractTest.methods.Subscribe(this.web3.utils.fromAscii("foyer"),nbmoins,nbmoins).encodeABI();
      var tx = {
        to : "0xeD93Ff78E86eE9a7E9D3d7EB5424668BF7bcf929",
        value: 0,
        gas: 2000000,

        gasPrice:   0 ,
        chainId: 1515,

        data : encoded
      };





      return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());


    }

public subscribeCov(address){
  const contractTest = new this.web3.eth.Contract(covoiturageabi, "0x660b883c119262C6b44cA9485d59Cfd9c0Ea5Fc3");


  let encoded = contractTest.methods.ReserveSeat(address).encodeABI();
  var tx = {
    to : "0xe25Eb7C55CA8c9d1d01F546762ac0bCE9e38642d",
    value: '0',
    gas: 2000000,
    gasPrice: '0',

    chainId: 1515,

    data : encoded
  };


  return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());


}

  public addVoyages(destination,_Nbr_Seats,_departureTime,_price){
    const contractTest = new this.web3.eth.Contract(covoiturageabi, "0x660b883c119262C6b44cA9485d59Cfd9c0Ea5Fc3");


    let encoded = contractTest.methods.AddCar(this.web3.utils.fromAscii(destination),_Nbr_Seats,_departureTime,_price).encodeABI();
    var tx = {
      to : "0xe25Eb7C55CA8c9d1d01F546762ac0bCE9e38642d",
      value: '0',
      gas: 2000000,
      gasPrice: '0',

      chainId: 1515,

      data : encoded
    };


    return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());


  }

public  generateTransaction(){


  const contractTest = new this.web3.eth.Contract(abi, "0x688912B208B6684613275B462AbE659e5334c2f8");
 let encoded = contractTest.methods.depositeScholarship( 1,localStorage.getItem("pubKey").toString()).encodeABI();

  var tx = {
    to : "0x688912B208B6684613275B462AbE659e5334c2f8",
    value: '0',
    gas: 2000000,
    gasPrice: '0',

    chainId: 1515,

    data : encoded
  };


 return  this.web3.eth.accounts.signTransaction(tx, localStorage.getItem("prvKey").toString());

}
  // @ts-ignore
  public async sendTransaction(address: string, amount: number) {
    const account = this.web3.eth.accounts.privateKeyToAccount('0x' + this.privateKey);
    this.web3.eth.accounts.wallet.add(account);
    this.web3.eth.defaultAccount = account.address;

    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    const params = {
      //nonce: 0,
      to: address,
      //from: this.accountAddress,
      value: this.web3.utils.toWei(amount, 'ether'),
      gasPrice: 5000000000,
      gasLimit: 21000,
      //chainId: 3
    };

    const transaction = await this.web3.eth.sendTransaction(params);
    return transaction.transactionHash;
  }

  // @ts-ignore
  public async signTransaction(address: string, amount: number) {
    // @ts-ignore
    // @ts-ignore
    const params = {
      to: address,
      value: this.web3.utils.toWei(amount, 'ether'),
      gasPrice: 5000000000,
      gasLimit: 21000,
    };
    const transaction = await this.web3.eth.accounts.signTransaction(params, '0x' + this.privateKey);
    return transaction;
  }


  public getPrivateKey() {
    return this.privateKey;
  }


  public getAccount () {
    return this.accountAddress;
  }
  public getMnemonic () {
    return this.mnemonic;
  }

  // @ts-ignore
  public async getGasPrice() {
    return await this.web3.eth.getGasPrice();
  }
public verify(privateKey){
 let data= this.web3.utils.utf8ToHex("hello");

  let signed_data = this.web3.eth.accounts.sign(data,privateKey);

return signed_data;/*
let v =signed_data.v;
  let r=signed_data.r;
  let s=signed_data.s;
  let l=this.web3.eth.accounts.recover({
    messageHash: signed_data.messageHash,
    v: v,
    r: r,
    s: s
  });
  console.log("result account"+l)
  console.log("account"+this.getAccount());

  console.log(JSON.stringify(signed_data))
  this.web3.eth.personal.ecRecover(data, signed_data.signature).then(console.log)
*/

}





/*





public verify(){
  var privateKey = eccrypto.generatePrivate();
  var publicKey = eccrypto.getPublic(privateKey);
console.log("account")
  this.ge
  console.log(this.getAccount())
  var str = "message to sign";
  var msg = crypto.createHash("sha256").update(str).digest();

  eccrypto.sign(privateKey, msg).then(function(sig) {
    console.log("Signature in DER format:", sig);
    eccrypto.verify(publicKey, msg, sig).then(function() {
      console.log("Signature is OK");
    }).catch(function() {
      console.log("Signature is BAD");
    });
  });




 */
  // @ts-ignore
  public async getChainId() {
    return await this.web3.eth.net.getId()
  }  
}
