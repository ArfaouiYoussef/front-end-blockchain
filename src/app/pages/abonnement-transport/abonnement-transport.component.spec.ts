import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbonnementTransportComponent } from './abonnement-transport.component';

describe('AbonnementTransportComponent', () => {
  let component: AbonnementTransportComponent;
  let fixture: ComponentFixture<AbonnementTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbonnementTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbonnementTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
