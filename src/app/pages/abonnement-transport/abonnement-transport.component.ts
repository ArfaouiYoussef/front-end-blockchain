import { Component, OnInit } from '@angular/core';
import {UserService} from "../../service/user-service.service";
import {MatAutocompleteSelectedEvent} from '@angular/material';
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import swal from 'sweetalert';
import {tokenReference} from "@angular/compiler";

@Component({
  selector: 'ngx-abonnement-transport',
  templateUrl: './abonnement-transport.component.html',
  styleUrls: ['./abonnement-transport.component.scss']
})
export class AbonnementTransportComponent implements OnInit {
  ListAbonnement: any;
  nbMoins: any;
  type: any;
  prix: any;
loading:any
  listAbonnement: Object;

  constructor(private abonnementService: UserService,private web3js:EthereumProvider) {
  this.loading=false;
  this.abonnementService.getAllAbonnementType().subscribe(res => {
      console.log(res)
      this.ListAbonnement = res;
    })
  }

  ngOnInit() {
  this.nbMoins=""
this.abonnementService.getAbonnementTransport().subscribe(res=>{
  this.listAbonnement=(res);
})
  }

  getType(event) {
    if (event.isUserInput ===true){
      this.type=event.source.value.type;
   this.prix=event.source.value.prix;
      console.log(event.source.value)
  }


}
  onSelectionChanged(event) {
    console.log(event.option.value.length);
  }

  displayFn(option: any) {
    
  }

  Subscribe() {
   this.loading=true;
    this.web3js.subscribeRestaurant(this.type,this.nbMoins,(this.nbMoins*this.prix)).then(signed=>{
      console.log(signed)
      this.abonnementService.subscribeTransport(signed.rawTransaction,this.prix).subscribe(res=>{
        console.log(res);
        this.loading=false;
        swal("", "Transaction Success!", "success");
        this.abonnementService.getAbonnementTransport().subscribe(res=>{
          this.listAbonnement=(res);
        })
      }, async error1 => {

this.loading=false;

        swal("Oops!", "Transaction Failed user exist!", "error");

      }
    )
    })

  }
}
