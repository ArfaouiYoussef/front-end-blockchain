import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../service/user-service.service";
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService
} from "@nebular/theme";
import {ToasterConfig} from "angular2-toaster";
import {AdminServiceService} from "../../service/admin-service.service";
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.scss']
})
export class AddAdminComponent implements OnInit {

  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  options= {
    fileSize: 2048, // in Bytes (by default 2048 Bytes = 2 MB)
    minWidth: 10, // minimum width of image that can be uploaded (by default 0, signifies any width)
    maxWidth: 10,  // maximum width of image that can be uploaded (by default 0, signifies any width)
    minHeight: 10,  // minimum height of image that can be uploaded (by default 0, signifies any height)
    maxHeight: 10,  // maximum height of image that can be uploaded (by default 0, signifies any height)
    fileType: ['image/gif', 'image/jpeg', 'image/png'], // mime type of files accepted
    height: 400, // height of cropper
    quality: 0.8, // quaity of image after compression
    crop: [  // array of objects for mulitple image crop instances (by default null, signifies no cropping)
      {
        ratio: 1, // ratio in which image needed to be cropped (by default null, signifies ratio to be free of any restrictions)
        minWidth: 10, // minimum width of image to be exported (by default 0, signifies any width)
        maxWidth: 10,  // maximum width of image to be exported (by default 0, signifies any width)
        minHeight: 10,  // minimum height of image to be exported (by default 0, signifies any height)
        maxHeight: 10,  // maximum height of image to be exported (by default 0, signifies any height)
        width: 10,  // width of image to be exported (by default 0, signifies any width)
        height: 10,  // height of image to be exported (by default 0, signifies any height)
      }
    ]
  };
  private selectedFile: any;
  private uploadForm: FormGroup;
   picture: any;
  constructor(private router:Router,private fb: FormBuilder,private adminService:AdminServiceService,private toastrService: NbToastrService) {
  }
  addUserForm: FormGroup;
  submitted = false;
  today=new Date();

  httpErrors: string;


  config: ToasterConfig;

  index = 1;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';

  title = 'HI there!';
  content = `I'm cool toaster!`;

  types: NbComponentStatus[] = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
  ];
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  quotes = [
    { title: null, body: 'We rock at Angular' },
    { title: null, body: 'Titles are not always needed' },
    { title: null, body: 'Toastr rock!' },
  ];
  isAfter: any;

  makeToast() {
    this.showToast(this.status, this.title, this.content);
  }

  openRandomToast () {
    const typeIndex = Math.floor(Math.random() * this.types.length);
    const quoteIndex = Math.floor(Math.random() * this.quotes.length);
    const type = this.types[typeIndex];
    const quote = this.quotes[quoteIndex];

    this.showToast(type, quote.title, quote.body);
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      `Message : ${titleContent}`,
      config);
  }


  ngOnInit() {
    this.picture='';
    this.uploadForm = this.fb.group({
      file: ['']
    });

    this.addUserForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      userStore: [false],

      userPhone: ['', Validators.required],
      userAddresse: ['', Validators.required],

      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      userPicture:['']
    }, {
    });
  }




  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    console.log(this.selectedFile);
    const uploadData = new FormData();
    uploadData.append('myFile', this.selectedFile, this.selectedFile.name);

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file').setValue(file);

      const formData = new FormData();
      formData.append('file', this.uploadForm.get('file').value);

      this.adminService.uploadImg(formData).subscribe(res => {
        console.log(res);
        this.picture = res.fileDownloadUri;
        this.addUserForm.controls['userPicture'].setValue(this.picture);


      })
    }
  }
  onReset() {
    this.router.navigate(["/dashboard/parametres"]);
  }

  onSubmit() {
    this.isAfter=true;
    console.log(this.addUserForm.value);
    if(this.addUserForm.valid){
      this.adminService.addAdmin(this.addUserForm.value).toPromise().then(res=>{
        this.toastrService.success("Succés");
        this.router.navigate(['/dashboard/parametres'])
      }).catch(err=>{
        this.toastrService.danger("Échec verifer votre adresse email");
      });
    }
  }
  get f() { return this.addUserForm.controls; }

}

