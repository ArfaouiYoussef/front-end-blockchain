import { Component, OnInit } from '@angular/core';
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {UserService} from "../../service/user-service.service";
import swal from 'sweetalert';

@Component({
  selector: 'ngx-bourse',
  templateUrl: './bourse.component.html',
  styleUrls: ['./bourse.component.scss']
})
export class BourseComponent implements OnInit {
  listSchoolar: Object;
  type: any;
  prix: any;
  listTranche:any;
lock:any;
  constructor(private  web3js: EthereumProvider, private userservice: UserService) {
 this.userservice.getuserBourse().subscribe((res:any)=>{
   console.log(res);
   if(res.length===2){
     this.lock=true;
   }
   this.listTranche=res;
   console.log(this.lock)
 })
  }

  ngOnInit() {

    this.web3js.getBourse("").then(signed => {
      console.log(signed);
    });

    this.userservice.getAllschoolarship().subscribe(res => {

      this.listSchoolar = (res)
    })
  }

  test() {

    this.web3js.getBourse("1").then(signed => {
      console.log(signed);
      this.userservice.getBourse(signed.rawTransaction,this.prix).subscribe((res:any) => {
        console.log(res.state);
        swal("", "Transaction Success!", "success");

        this.userservice.getuserBourse().subscribe((res:any)=>{

          if(res.length===2){
            this.lock=true;
          }
          this.listTranche=res;
          console.log(this.lock)
        })
      },error1 => {
        swal("", "s'il vous plaît contactez administrateur!", "error");

      });

    })


  }



  getType(event) {
    if (event.isUserInput === true) {
      this.type = event.source.value.type;
      this.prix = event.source.value.prix;
      console.log(event.source.value)
    }




  }
}
