import { Component, OnInit } from '@angular/core';
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {UserService} from "../../service/user-service.service";
import swal from "sweetalert";

@Component({
  selector: 'ngx-covoiturage',
  templateUrl: './covoiturage.component.html',
  styleUrls: ['./covoiturage.component.scss']
})
export class CovoiturageComponent implements OnInit {
  type: any;
  prix: any;
  list: {};
userexist:any;
  destination: any;
  nbPlace: any;
  departTime: any;
  price: any;
  loading: boolean;
  listDestination: any;
  books: any;
  userType: any;
  submit: boolean;

  constructor(private web3js:EthereumProvider,private covService:UserService) {
this.list=[{type:"chauffeur"},{type:"passager"}];

this.submit=false;



  }

  getType(event) {
    if (event.isUserInput ===true){
      console.log(event.source.value.type);
      this.type=event.source.value.type;
    }


  }
  subscribeCov(address,id){
    this.submit=true;
    this.web3js.subscribeCov(address).then(signed=>{
this.covService.subscribeCov(signed.rawTransaction,address).subscribe(res=>{

  swal("", "Transaction effectuée avec succès ", "success");
  this.submit=false;

  console.log(res);
  this.listDestination=this.listDestination.filter(
    item => item._id != id);

},error1 => {
  swal("", "Transaction échouée !", "error");
  this.submit=false;

})


    });
  }
  ngOnInit() {
    this.covService.getUserCov().subscribe((res:any)=>{

      console.log(res)
      this.userType=res.type;
    });
this.loading=true;
this.web3js.checkUser().then(signed=>{
this.covService.checkUser(signed.rawTransaction).subscribe(res=>{
  console.log(res);
this.loading=false;
  this.userexist=false;

  },err=>{
  this.userexist=true;
  this.loading=false;
})


});

this.covService.getAllDestination().subscribe(res=>{
  this.listDestination=res;
})
  }

  adduser() {
this.loading=true;
    this.web3js.addUserCovoiturage(this.type).then(signed=>{

      this.covService.addUserCovoiturage(signed.rawTransaction,this.type).subscribe(res=>{
        swal("", "Transaction effectuée avec succès ", "success");

        this.loading=false;
        console.log(res);
        this.userexist=true;
      },err=>{
        this.loading=false;
        swal("", "Transaction échouée !", "error");

      })
    })

  }

  addDestination(){
this.loading=true;
    this.web3js.addVoyages(this.destination,this.nbPlace,this.departTime,this.price).then(signed=>{

      this.covService.addDestination(signed.rawTransaction,this.destination,this.nbPlace,this.price,this.departTime).subscribe(res=>{
  this.loading=false;
        console.log(res);
        swal("", "Transaction effectuée avec succès ", "success");

      }, async error1 => {

        this.loading=false;

        swal("", "Transaction échouée !", "error");

      })

    })




}

}
