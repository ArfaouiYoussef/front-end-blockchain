import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OonuComponent } from './oonu.component';

describe('OonuComponent', () => {
  let component: OonuComponent;
  let fixture: ComponentFixture<OonuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OonuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OonuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
