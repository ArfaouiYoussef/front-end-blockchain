import { Component, OnInit } from '@angular/core';
import {UserService} from "../../service/user-service.service";
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import swal from "sweetalert";

@Component({
  selector: 'ngx-oonu',
  templateUrl: './oonu.component.html',
  styleUrls: ['./oonu.component.scss']
})
export class OonuComponent implements OnInit {
  listTypeFoyer: Object;
nbMoins:any;
  reservation: any;
  loading: boolean;

  constructor(private service:UserService,private web3js:EthereumProvider) { }

  ngOnInit() {
this.service.getFoyerByUser().subscribe(res=>{
  this.reservation=(res);
  console.log(res)
})

 }

  OnReset() {

  }

  subscribeFoyer() {
    if (this.nbMoins > 0) {
  this.loading=true;
      this.web3js.subscribeFoyer(this.nbMoins).then(signed => {
        console.log(signed);
        this.service.subscribeFoyer(signed.rawTransaction,this.nbMoins).subscribe(res=>{
          swal("", "Transaction effectuée avec succès ", "success");
          this.loading=false;

          console.log(res);
          this.service.getFoyerByUser().subscribe(res=>{
            this.reservation=(res);
            console.log(res)
          })

        })
      },error=>{
        this.loading=false;

        swal("", "Transaction échouée !", "error");

      });
    }
  }
}
