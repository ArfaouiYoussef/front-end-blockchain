import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Tableau de bord\n',
  },
  {
    title: 'Service oonu',
    icon: 'credit-card-outline',
    children: [

      {
        title: 'Bourse',
        link: '/dashboard/bourse',

      },
      {
        title: 'Foyer',
        link: '/dashboard/foyer',

      },

      {
        title: 'Restaurant Universitaire',
        link: '/dashboard/restaurant',

      }
    ],
  },{
    title: 'Transport',
    icon: 'car-outline',
    children: [
      {
        title: 'Abonnement',
        link: '/dashboard/abonnementTransport',
      },
      { title: 'Co-voiturage',
        link: '/dashboard/covoiturage',

      }
    ],
  },
  {
    title: 'Compte ',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Solde',
        link: '/dashboard/promotion/liste-promotion',
      }
    ],
  }

];
