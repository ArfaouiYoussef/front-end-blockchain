import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import {ECommerceComponent} from './profil/e-commerce.component';
import {AddAdminComponent} from "./add-admin/add-admin.component";
import {OonuComponent} from "./oonu/oonu.component";
import {BourseComponent} from "./bourse/bourse.component";
import {RestaurantComponent} from './restaurant/restaurant.component';
import {AbonnementTransportComponent} from "./abonnement-transport/abonnement-transport.component";
import {CovoiturageComponent} from "./covoiturage/covoiturage.component";
//
const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
 {
      path: 'profile',
      component: ECommerceComponent,
    },{
    path:'bourse',
      component:BourseComponent,
    },

    {
      path:'foyer',
      component:OonuComponent,
    },
    {
      path:'restaurant',
      component:RestaurantComponent,
    },

    {
      path:'abonnementTransport',
      component:AbonnementTransportComponent,
    },
    {
      path:"covoiturage",
      component:CovoiturageComponent
    },


    {
      path: '',
      redirectTo: 'profile',
      pathMatch: 'full',
    },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
