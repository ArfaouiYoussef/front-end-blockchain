import { NgModule } from '@angular/core';
import {NbCardModule, NbMenuModule} from '@nebular/theme';
import { FormsModule } from '@angular/forms';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDividerModule, MatExpansionModule,
  MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule, MatTabsModule
} from "@angular/material";
import {ECommerceModule} from './profil/e-commerce.module';
import { ParametresComponent } from './parametres/parametres.component';
import {ReactiveFormsModule} from "@angular/forms";
import { AddAdminComponent } from './add-admin/add-admin.component';
import { OonuComponent } from './oonu/oonu.component';
import { BourseComponent } from './bourse/bourse.component';
import { TransportComponent } from './transport/transport.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { AbonnementTransportComponent } from './abonnement-transport/abonnement-transport.component';
import { CovoiturageComponent } from './covoiturage/covoiturage.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    MatDatepickerModule,
    ECommerceModule,
    FormsModule,
    MatButtonModule,
    MatDatepickerModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    NbCardModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTabsModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    PagesComponent,
    AddAdminComponent,
    OonuComponent,
    BourseComponent,
    TransportComponent,
    RestaurantComponent,
    AbonnementTransportComponent,
    CovoiturageComponent,


  ],
})
export class PagesModule {
}
