import {Component, OnInit, TemplateRef} from '@angular/core';
import {Observable} from "rxjs";
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../../service/user-service.service";
import {NbDialogService, NbToastrService} from "@nebular/theme";
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {AdminServiceService} from "../../service/admin-service.service";

@Component({
  selector: 'ngx-parametres',
  templateUrl: './parametres.component.html',
  styleUrls: ['./parametres.component.scss']
})
export class ParametresComponent implements OnInit {

  users: any = [];

  userList: any = [];
  filteredUsers: Observable<any[]>;
  myControl = new FormControl();
  options:[] = [];
  filteredOptions: Observable<string[]>;
currentAccountId:any;
  constructor(private router:Router,private adminService:AdminServiceService,private dialogService: NbDialogService,private toastrService: NbToastrService) {
    this.adminService.getAllAdmin().subscribe((response: any) => {
this.currentAccountId=JSON.parse(localStorage.getItem("user")).admin_id;

      this.userList=(response.admins);
      console.log(this.userList);
      this.options=response.admins;

    });
  }

  panelOpenState: boolean=false;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.options.slice())
      );
  }

  displayFn(user?: any): string | undefined {
    return user ? user.username : undefined;
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    this.users= this.options.filter((option:any) => option.username.toLowerCase().indexOf(filterValue) === -1);
    console.log(this.userList);
    return this.userList=this.options.filter((option:any) => option.username.toLowerCase().indexOf(filterValue) === 0);
  }




  delete(id) {
    this.adminService.deleteAdmin(id).subscribe(res=>{
      console.log(res);
      this.toastrService.success("Delete success ");

    },error=>{
      console.log(error);
      this.toastrService.danger("Delete Failed !!! ");
    });
  }


  names: string[] = [];


  open() {
    this.dialogService.open(null, {
      context: {
        title: 'Delete',
      },
    });

  }

  open2(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      { context: 'Delete' });
  }
  removeUser(user: any): void {
    this.adminService.deleteAdmin(user.user_id).subscribe(res => {
      this.userList = this.userList.filter(t => t.user_id !== user.user_id);
      this.options=this.userList;
      this.toastrService.success("success");
    },error1 => this.toastrService.danger("Failed"));
  }



  reset() {
    if(this.myControl.value===''){
      this.userList= this.options.filter((option:any) => option.username.toLowerCase().indexOf('') === 0);
      console.log("users"+this.userList)}
  }

  selectedTag(event: MatAutocompleteSelectedEvent): void {
    const tag: any = event.option.value;
    this.userList = this.userList.filter(item => item.username == tag.username);
  }


  navAddUser() {
    this.router.navigate(['/dashboard/ajouter-admin']);
  }

  navProfil() {
    this.router.navigate(["/dashboard/profile"]);
  }
}

