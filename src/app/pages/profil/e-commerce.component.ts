import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../service/user-service.service";
import {NbToastrService} from "@nebular/theme";
import {ActivatedRoute, Router} from "@angular/router";
import {AdminServiceService} from "../../service/admin-service.service";
import {MatSlideToggleChange} from "@angular/material";
import {Subject} from "rxjs";
import {MessageService} from "../../service/message.service";
import {EthereumProvider} from "../../ethereumProvider/ethereum";

@Component({
  selector: 'ngx-ecommerce',
  templateUrl: './e-commerce.component.html',
  styleUrls: ['./e-commerce.component.scss'],

})
export class ECommerceComponent {
  private selectedFile: any;
  private uploadForm: any;
   picture: any;
   adminForm: FormGroup;
  private admin: any;
  private admin_id: any;
  solde: any;

  constructor(private router:Router,private fb: FormBuilder,private adminService:AdminServiceService,

              private eth:EthereumProvider,

              private messageService:MessageService,private toastrService: NbToastrService,private userService:UserService,               private activatedRoute: ActivatedRoute) {





  }
  gettoken() {
    this.userService.getTOKEN();
 this.messageService.getMessage().subscribe(message => {
      this.solde = message.token;
      });
  }
  ngOnInit() {
    this.uploadForm = this.fb.group({
      file: ['']
    });
    this.adminForm = this.fb.group({

      user_id: [''],

      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],

      userPhone: ['', Validators.required],
      userAddresse: ['', Validators.required],

      password: ['', [Validators.minLength(6)]],
      confirmPassword: [''],
      userPicture:['']
    }, {
    });

    this.gettoken();

this.adminForm.patchValue({

  username:localStorage.getItem("pubKey")

})

  }



  onFileChanged(event) {
    console.log(event)
    this.selectedFile = event.target.files[0]
    console.log(this.selectedFile);
    const uploadData = new FormData();
    uploadData.append('myFile', this.selectedFile, this.selectedFile.name);

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file').setValue(file);

      const formData = new FormData();
      formData.append('file', this.uploadForm.get('file').value);
      console.log(formData)
      var encodedData = btoa(file);
      console.log(atob(encodedData))

      /*   this.userService.uploadImg(formData).subscribe(res=>{
           console.log(res);
           this.picture=res.fileDownloadUri;
           this.adminForm.controls['userPicture'].setValue(this.picture);

           console.log(this.adminForm.value)

         })*/
    }


  }


  onSubmit() {

    if(this.adminForm.valid){
      this.adminService.updateAdmin(this.adminForm.value).toPromise().then(res=>{
        this.toastrService.success("Succés","  ");
        this.messageService.sendMessage(this.picture,this.adminForm.controls["username"].value);
      }).catch(err=>{
        console.log(err.ErrorInterceptor);

      });
    }
    else{
      console.log(this.adminForm.value);

    }
  }
  get f() { return this.adminForm.controls; }

  OnReset() {
    this.router.navigate(["/dashboard/parametres"]);
  }

  testme() {
    this.eth.getTokenBalance().then(res=>{

      console.log(res)
    });
  }
}
