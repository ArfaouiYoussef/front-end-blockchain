import { Component, OnInit } from '@angular/core';
import {EthereumProvider} from "../../ethereumProvider/ethereum";
import {UserService} from "../../service/user-service.service";
import swal from "sweetalert";

@Component({
  selector: 'ngx-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {
  nbmoins: string;
  listAbonnement: Object;
  loading: boolean;

  constructor(private restaurantservice: UserService, private web3js: EthereumProvider) {


  }

  ngOnInit() {
    this.restaurantservice.getAbonnementRestaurant().subscribe(res=>{console.log(res)

      this.listAbonnement=res;


    })
  }

  test() {

  }

  subscribeTest() {
    this.loading=true;
    console.log(this.nbmoins);
    this.web3js.subscribeResto(this.nbmoins).then(signed => {
      console.log(signed)
      this.restaurantservice.subscribeRestaurant(signed.rawTransaction,this.nbmoins).subscribe(response => {
        console.log(response)
        this.loading=false;

        this.restaurantservice.getAbonnementRestaurant().subscribe(res=>{console.log(res)
          swal("", "Transaction effectuée avec succès ", "success");

          this.listAbonnement=res;


        },error1 => {
          this.loading=false;
          swal("", "Transaction échouée !", "error");

        })
      },error1 =>           this.loading=false
    )


    });
  }
}
