import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  constructor(private  httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }


  getAdminById(user_id){
    return this.httpClient.post("/api/admin/user_account_by_id",
      {
        "userId": user_id

      })
  }

getAllAdmin(){
  return  this.httpClient.get('/api/admin/getAll')

}
  updateAdmin(user:any){
    return  this.httpClient.post('/api/admin/update_user_profile',user)

  }

  addAdmin(admin:any){
    return this.httpClient.post("/api/admin/register",admin);
  }

  deleteAdmin(admin_id:any)
  {
    return    this.httpClient.delete("/api/admin/"+admin_id);}


  uploadImg(data){


    return  this.httpClient.post<any>("/api/users/uploadFile", data
    );
  }
}
