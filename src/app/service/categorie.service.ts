import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CategorieService {


  constructor(private  httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }
  getAllCategories(){
    return this.httpClient.get("/api/categorie/getAll_categorie")
  }
  addCategorie(categorie){
    return this.httpClient.post("/api/categorie/add_categorie",categorie);
  }

  uploadImg(data){


    return  this.httpClient.post<any>("/api/products/uploadProductImage", data
    );
  }
  deleteCategorie(categorie: any) {
    return this.httpClient.post("/api/categorie/delete_categorie",
      categorie
    )

  }


  getCategorieById(id:any){
    return   this.httpClient.post("/api/categorie/getCategorieById",{id:id});
  }
  updateCategorie(categorie) {

   return  this.httpClient.post("/api/categorie/update_categorie",categorie);
  }
 // -------------sous Categorie----------------
  addSousCategorie(sousCategorie: any){
  return   this.httpClient.post("/api/sous_categorie/addSousCategorie",sousCategorie);
  }

  //-store categorie
  addStoreCategorie(storeCategorie){
    return this.httpClient.post("/api/store_categories/addStoreCategories",storeCategorie);
  }
getStoreCategorieByStoreId(id:any){
   return  this.httpClient.post("/api/store_categories/get_store_categorieByStoreId",
      {storeId:id})
}
deleteStoreCategorie(storeid,subCategoreId){
    return this.httpClient.post("/api/store_categories/delete_store_categorie",{
      storeId:storeid,
      subCategorieId:subCategoreId
    })
}
  getSousCategorie(id:any){
  return   this.httpClient.post("/api/sous_categorie/get_sous_categorie_byCategorieId",{
    categorie_id:id
  });
  }

  getSousCategorieById(id:any){
   return  this.httpClient.post("/api/sous_categorie/get_sous_categorie_byId",{
      sous_categorie_id:id
    })
  }
  updateSousCategorie(sousCategorie){
   return  this.httpClient.post("/api/sous_categorie/update_sous_categorie",sousCategorie)
  }
  getAllSousCategorie(){
   return  this.httpClient.get("/api/sous_categorie/all_sous_categorie");
  }


  deleteSousCategorie(id:any){
   return  this.httpClient.post("/api/sous_categorie/delete_sous_categorie",{
      sous_categorie_id:id
    })
  }


}
