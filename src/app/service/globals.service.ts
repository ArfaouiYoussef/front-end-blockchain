import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalsService {
  private user: any;

  constructor() {

  }

   getUser() {
    return this.user
  }

   setUser(user) {
this.user=user

  }

}
