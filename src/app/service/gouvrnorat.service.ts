import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GouvrnoratService {




  constructor(private  httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }
  getAllGouvernorat(){
    return this.httpClient.get("/api/gouvernorat/getAll");
  }
  addZone(zone){
    return this.httpClient.post("/api/zone/add_zone",zone);

  }
  updateZone(zone){
    return this.httpClient.post("/api/zone/update_zone",{
      gouvernoratId:zone.gouvernoratId,
      zoneId:zone.id,
      nomZone:zone.nomZone,

    });

  }


  addSecteur(secteur){
    return this.httpClient.post("/api/secteurs/add_secteur",secteur);
  }
  updateSecteur(secteur){
    return this.httpClient.post("/api/secteurs/update_secteur",secteur);
  }

  getSecteurById(id:any){
    return this.httpClient.post("/api/secteurs/get_secteur_ById",{secteurId:id});

  }


deleteSecteur(secteur){
 return    this.httpClient.post("/api/secteurs/delete_secteur_ById",{secteurId:secteur.id})
}
  getAllSecteurs(){
   return  this.httpClient.get("/api/secteurs/get_all_secteur");
  }
  deleteZone(zone){
    return this.httpClient.post("/api/zone/delete_zone",{"zoneId":zone.id});
  }
  getZoneByName(zone){
   return  this.httpClient.post("/api/zone/get_zone_ByZoneName",{nomZone:zone});
  }
  updateGouvernorat(gouvernorat){
  return   this.httpClient.post("/api/gouvernorat/update_gouvernorat",gouvernorat);
  }
  getById(id){
   return  this.httpClient.post("/api/gouvernorat/get_gouvernoratById",{
      id:id
    })
  }
  getZoneByGouvernorat(id){
   return  this.httpClient.post("/api/zone/get_all_zone_ByGouvernoratId",{gouvernoratId:id})
  }
  deleteGouvernorat(gouvernorat){
   return  this.httpClient.post("/api/gouvernorat/delete_gouvernorat",gouvernorat);
  }
 addGouvernorat(gouvernorat){
  return   this.httpClient.post("/api/gouvernorat/add_gouvernorat",gouvernorat)
 }


}
