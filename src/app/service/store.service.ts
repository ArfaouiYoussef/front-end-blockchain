import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class StoreService {


  constructor(private  httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }

  addPromo(user){
    return    this.httpClient.post("/api/store/store_add_promotion",
      user);}

  getAllStores(){
    return    this.httpClient.get("/api/users/getAllStores");}

getAllPromotion()
{
  return    this.httpClient.get("/api/store/All_Promotion");

  }



  deleteUser(promotionId)
  {
    return    this.httpClient.post("/api/store/store_delete_promotion/",{
      "promotionId":promotionId
    });}
addUserPoints(userPoints){
   return  this.httpClient.post("/api/users_points/user_add_points",userPoints);
}
getUserByStoreId(storeId)
{
 return  this.httpClient.post("/api/users_points/user_store",{storeId:storeId})
}
updatePoints(userPoints){
    return this.httpClient.post("/api/users_points/user_delete_points",userPoints);
}
  deleteStore(storeId)
  {
    return    this.httpClient.delete("/api/users/"+storeId);
  }

  updatePromotionAlbumById(promotionAlbum){
    return this.httpClient.post("/api/promotion_album/update_promotion_album",

      promotionAlbum)
  }
  deleteAlbumById(id:any){
    return this.httpClient.post("/api/promotion_album/delete_promotion_album",

      {promotionAlbumId:id})

  }


  updatePack(pack){
   return  this.httpClient.post("/api/packs/update_pack",pack);
  }
  getPackByStoreId(id:any){

    return this.httpClient.post("/api/packs/get_Pack_ByStoreId",{id:id})  }

  addPack(pack){
    return this.httpClient.post("/api/packs/add_pack",pack)  }

  getPromoById(promotionId){
  return this.httpClient.post("/api/store/promotion_by_id",
    {
      "promotionId": promotionId

    })

}
updatePromotion(promotion){
  return    this.httpClient.post("/api/store/store_update_promotion/",
    promotion  );}

  getUserById(user_id){
    return this.httpClient.post("/api/users/user_account_by_id",
      {
        "userId": user_id

      })
  }

  updateUser(user:any){
    console.log('postusers'+user)
    return  this.httpClient.post('/api/users/update_user_profile',user)

  }
  uploadImg(data){


    return  this.httpClient.post<any>("/api/users/uploadFile", data
    );
  }

  getAlbumById(id){
    return this.httpClient.post("/api/promotion_album/get_promotion_album_byId",

      {promotionAlbumId:id})
  }

  getAlbumsByPromotionId(id:any){
    return    this.httpClient.post("/api/promotion_album/get_promotion_album_by_promotionId",{"promotionId":id});

  }
addALbum(album){
    return this.httpClient.post("/api/promotion_album/add_promotion_album",album);

}
  uploadPromotionAlbumImg(data){


    return  this.httpClient.post<any>("/api/promotion_album/uploadPromotionAlbumImage", data
    );
  }

  addProduct(value: any) {
    
  }
}
