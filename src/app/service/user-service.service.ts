import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EthereumProvider} from "../ethereumProvider/ethereum";
import {MessageService} from "./message.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private  httpClient: HttpClient,
              private web3js:EthereumProvider,
              private msg:MessageService) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }

  addUser(email,address){
    return    this.httpClient.post("http://5.135.52.74:3000/register",
      ({ username: 'ddm', email: email,address:address,photo:"", password: "25" }));}

getAllschoolarship(){
    return this.httpClient.get("http://5.135.52.74:3000/getAllSchoolarShip");


  }
  getAllFoyerType(){
    return this.httpClient.get("http://5.135.52.74:3000/getAllFoyerType");


  }
  getAllAbonnementType(){
    return this.httpClient.get("http://5.135.52.74:3000/getAllAbonnement");

  }
  getTOKEN() {

    this.web3js.getTokenBalance().then(res=>{
this.msg.sendMessage(res,"token");
      console.log(res)
    });
  }
getUserCov(){

  return    this.httpClient.post("http://5.135.52.74:3000/getUserCov/",
    ({ address:localStorage.getItem("pubKey")}));

}


  subscribeTransport(data,prix){
    console.log(data)
    return    this.httpClient.post("http://5.135.52.74:3000/subscribeTransport",
      ({ transaction: data,address:localStorage.getItem("pubKey").toString(),nbMoins:prix}));
  }
  addUserCovoiturage(data,type){
    console.log(data)
    return    this.httpClient.post("http://5.135.52.74:3000/addUserCovoiturage",
      ({ transaction: data,address:localStorage.getItem("pubKey").toString(),type:type}));
  }

login(email,signed_data){
  return    this.httpClient.post("http://5.135.52.74:3000/login",
    ({ username: 'ddm', email: email,signed_data:signed_data }));}
sendTransaction(data){
console.log(data)
  return    this.httpClient.post("http://5.135.52.74:3000/sendTransaction/",
    ({ transaction: data}));
}
getAbonnementRestaurant(){

  return    this.httpClient.post("http://5.135.52.74:3000/getAbonnementRestaurant/",
    ({ address:localStorage.getItem("pubKey")}));

}
  getAbonnementTransport(){

    return    this.httpClient.post("http://5.135.52.74:3000/getAbonnementTransport/",
      ({ address:localStorage.getItem("pubKey")}));

  }

  subscribeRestaurant(data,nbMoins){
    console.log(data);
    return    this.httpClient.post("http://5.135.52.74:3000/subscribeRestaurant/",
      ({ transaction: data,address:localStorage.getItem("pubKey").toString(),nbMoins:nbMoins}));
  }

  getBourse(data,value){
    return    this.httpClient.post("http://5.135.52.74:3000/getBourse/",
      ({ transaction:data,address:localStorage.getItem("pubKey").toString(),value:value}));
  }
subscribeFoyer(data,nbmoins){
  return    this.httpClient.post("http://5.135.52.74:3000/subscribeFoyer/",
    ({ transaction:data,address:localStorage.getItem("pubKey"),nbMoins:nbmoins}));

}
getFoyerByUser(){
  return    this.httpClient.post("http://5.135.52.74:3000/getFoyerByAddress/",{
    address:localStorage.getItem("pubKey").toString()
  });

}
getuserBourse(){

  return    this.httpClient.post("http://5.135.52.74:3000/getBourseSolde/",
    ({ address:localStorage.getItem("pubKey").toString()}));

}



  checkUser(data){
    console.log(data)
    return    this.httpClient.post("http://5.135.52.74:3000/checkUserCov/",
      ({ address: localStorage.getItem("pubKey").toString()}));
  }

getAllDestination(){
  return    this.httpClient.get("http://5.135.52.74:3000/getAllDestination/");

}
addDestination(data,destination,nbPlace,prix,date){
  return    this.httpClient.post("http://5.135.52.74:3000/addDestination/",
    ({ transaction: data,address:localStorage.getItem("pubKey").toString(),destination:destination,nbPlace:nbPlace,prix:prix,date:date}));

}
subscribeCov(data,addrDest)
{
  return    this.httpClient.post("http://5.135.52.74:3000/reservation/",
    ({ transaction: data,address:localStorage.getItem("pubKey").toString(),addressDestination:addrDest}));


}
  uploadImg(data){


    return  this.httpClient.post<any>("/api/users/uploadFile", data
   );
  }
}
